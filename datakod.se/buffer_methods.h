#pragma once

#include <cstring>

namespace datakod_se::buffer {

    namespace buffer_helpers
    {
        template <typename T>
        static inline void assert_bufsize_runtime(const T& buf, std::size_t expected_size)
        {
            // Check size of buffer in runtime
            if (buf.size() < expected_size) {
                throw IndexOutOfBoundsException();
            }
        }

        template <std::size_t expected_size, typename T>
        static inline void assert_bufsize_compile_time(const T& buf)
        {
            // Check size of buffer in compile time if possible, otherwise runtime
            static_assert(!T::is_fixed || T::fixed_size >= expected_size, "Index out of bounds");
            if (!T::is_fixed) {
                assert_bufsize_runtime(buf, expected_size);
            }
        }

        static inline uint16_t unpack_u8(const uint8_t * src)
        {
            return src[0];
        }

        static inline void pack_u8(uint8_t * dst, uint8_t value)
        {
            dst[0] = value;
        }

        static inline uint16_t unpack_u16(const uint8_t * src)
        {
            return (uint16_t(src[0]) << 8) | src[1];
        }

        static inline void pack_u16(uint8_t * dst, uint16_t value)
        {
            dst[0] = (value & 0xff00) >> 8;
            dst[1] = (value & 0x00ff) >> 0;
        }

        static inline uint32_t unpack_u32(const uint8_t * src)
        {
            return (uint32_t(src[0]) << 24) |
                   (uint32_t(src[1]) << 16) |
                   (uint32_t(src[2]) <<  8) |
                   (uint32_t(src[3]) <<  0);
        }

        static inline void pack_u32(uint8_t * dst, uint32_t value)
        {
            dst[0] = (value & 0xff000000) >> 24;
            dst[1] = (value & 0x00ff0000) >> 16;
            dst[2] = (value & 0x0000ff00) >>  8;
            dst[3] = (value & 0x000000ff) >>  0;
        }

        static inline uint48_t unpack_u48(const uint8_t * src)
        {
            return (uint48_t(src[0]) << 40) |
                   (uint48_t(src[1]) << 32) |
                   (uint48_t(src[2]) << 24) |
                   (uint48_t(src[3]) << 16) |
                   (uint48_t(src[4]) <<  8) |
                   (uint48_t(src[5]) <<  0);
        }

        static inline void pack_u48(uint8_t * dst, uint48_t value)
        {
            dst[0] = (value & 0xff0000000000) >> 40;
            dst[1] = (value & 0x00ff00000000) >> 32;
            dst[2] = (value & 0x0000ff000000) >> 24;
            dst[3] = (value & 0x000000ff0000) >> 16;
            dst[4] = (value & 0x00000000ff00) >>  8;
            dst[5] = (value & 0x0000000000ff) >>  0;
        }

        static inline uint64_t unpack_u64(const uint8_t * src)
        {
            return (uint64_t(src[0]) << 56) |
                   (uint64_t(src[1]) << 48) |
                   (uint64_t(src[2]) << 40) |
                   (uint64_t(src[3]) << 32) |
                   (uint64_t(src[4]) << 24) |
                   (uint64_t(src[5]) << 16) |
                   (uint64_t(src[6]) <<  8) |
                   (uint64_t(src[7]) <<  0);
        }

        static inline void pack_u64(uint8_t * dst, uint64_t value)
        {
            dst[0] = (value & 0xff00000000000000) >> 56;
            dst[1] = (value & 0x00ff000000000000) >> 48;
            dst[2] = (value & 0x0000ff0000000000) >> 40;
            dst[3] = (value & 0x000000ff00000000) >> 32;
            dst[4] = (value & 0x00000000ff000000) >> 24;
            dst[5] = (value & 0x0000000000ff0000) >> 16;
            dst[6] = (value & 0x000000000000ff00) >>  8;
            dst[7] = (value & 0x00000000000000ff) >>  0;
        }

        template <typename T, bool is_const, bool is_fixed>
        struct methods;

        template <typename T>
        struct methods<T, true, true>
        {
            // take()-methods for `FixedConstBuffer<M>'
            static constexpr std::size_t M = T::fixed_size;

            template <std::size_t I>
            static inline FixedConstBuffer<M-I>  skip(const T& buf)                                         { assert_bufsize_compile_time<M - I>(buf); return { get_raw(buf) + I }; }
            static inline ConstBuffer            skip(const T& buf, std::size_t I)                          { assert_bufsize_runtime(buf, I); return { get_raw(buf) + I, buf.size() - I }; }

            template <std::size_t I, std::size_t N>
            static inline FixedConstBuffer<N>    take_fixed(const T& buf)                                   { assert_bufsize_compile_time<I + N>(buf); return { get_raw(buf) + I }; } 

            static inline ConstBuffer            take_non_fixed(const T& buf, std::size_t N)                { assert_bufsize_runtime(buf, N); return { get_raw(buf), N }; } 
            static inline ConstBuffer            take_non_fixed(const T& buf, std::size_t I, std::size_t N) { assert_bufsize_runtime(buf, I + N); return { get_raw(buf) + I, N }; }

            static inline FixedConstBuffer<M>    as_const(const T& buf)                                     { return { get_raw(buf) }; }
        };

        template <typename T>
        struct methods<T, false, true>
        {
            // take()-methods for `FixedBuffer<M>'
            static constexpr std::size_t M = T::fixed_size;

            template <std::size_t I>
            static inline FixedBuffer<M-I>       skip(T& buf)                                               { assert_bufsize_compile_time<M - I>(buf); return { get_raw(buf) + I }; }
            static inline Buffer                 skip(T& buf, std::size_t I)                                { assert_bufsize_runtime(buf, I); return { get_raw(buf) + I, buf.size() - I }; }


            template <std::size_t I, std::size_t N>
            static inline FixedBuffer<N>         take_fixed(T& buf)                                         { assert_bufsize_compile_time<I + N>(buf); return { get_raw(buf) + I }; }
            static inline Buffer                 take_non_fixed(T& buf, std::size_t N)                      { assert_bufsize_runtime(buf, N); return { get_raw(buf), N }; }
            static inline Buffer                 take_non_fixed(T& buf, std::size_t I, std::size_t N)       { assert_bufsize_runtime(buf, I + N); return { get_raw(buf) + I, N }; }

            static inline FixedConstBuffer<M>    as_const(const T& buf)                                     { return { get_raw(buf) }; }
        };

        template <typename T>
        struct methods<T, true, false>
        {
            // take()-methods for `ConstBuffer'

            template <std::size_t I>
            static inline ConstBuffer            skip(const T& buf)                                         { assert_bufsize_runtime(buf, I);   return { get_raw(buf) + I, buf.size() - I }; }
            static inline ConstBuffer            skip(const T& buf, std::size_t I)                          { assert_bufsize_runtime(buf, I);   return { get_raw(buf) + I, buf.size() - I }; }

            template <std::size_t I, std::size_t N>
            static inline FixedConstBuffer<N>    take_fixed(const T& buf)                                   { assert_bufsize_runtime(buf, I+N);   return { get_raw(buf) + I }; }
            static inline ConstBuffer            take_non_fixed(const T& buf, std::size_t N)                { assert_bufsize_runtime(buf, N);     return { get_raw(buf), N }; }
            static inline ConstBuffer            take_non_fixed(const T& buf, std::size_t I, std::size_t N) { assert_bufsize_runtime(buf, I+N);   return { get_raw(buf) + I, N }; }

            static inline ConstBuffer            as_const(const T& buf)                                     { return { get_raw(buf), buf.size() }; }
        };

        template <typename T>
        struct methods<T, false, false>
        {
            // take()-methods for `Buffer'
            template <std::size_t I>
            static inline Buffer                 skip(T& buf)                                               { assert_bufsize_runtime(buf, I);     return { get_raw(buf) + I, buf.size() - I }; }
            static inline Buffer                 skip(T& buf, std::size_t I)                                { assert_bufsize_runtime(buf, I);     return { get_raw(buf) + I, buf.size() - I }; }

            template <std::size_t I, std::size_t N>
            static inline FixedBuffer<N>         take_fixed(T& buf)                                         { assert_bufsize_runtime(buf, I+N);   return { get_raw(buf) + I }; }
            static inline Buffer                 take_non_fixed(T& buf, std::size_t N)                      { assert_bufsize_runtime(buf, N);     return Buffer { get_raw(buf), N }; } 
            static inline Buffer                 take_non_fixed(T& buf, std::size_t I, std::size_t N)       { assert_bufsize_runtime(buf, I + N); return Buffer { get_raw(buf) + I, N }; }

            static inline ConstBuffer            as_const(const T& buf)                                     { return { get_raw(buf), buf.size() }; }
        };

        template <typename T>
        struct get_methods_type
        {
            using U    = std::remove_reference_t<std::remove_const_t<T>>;
            using type = buffer_helpers::methods<U, U::is_const, U::is_fixed>;
        };

        template <typename T>
        struct get_const_methods_type
        {
            using U    = std::remove_reference_t<std::remove_const_t<T>>;
            using type = buffer_helpers::methods<U, true, U::is_fixed>;
        };

        template <typename T>
        using methods_t = typename get_methods_type<T>::type;

        template <typename T>
        using const_methods_t = typename get_const_methods_type<T>::type;
    }

#define DECLARE_BUFFER_GETTERS_SETTERS(name_, type_, size_)   \
        template <typename T>            inline type_  get_ ## name_ (const T& buf)                           { buffer_helpers::assert_bufsize_compile_time<size_>(buf); return buffer_helpers::unpack_ ## name_(get_raw(buf)); } \
        template <typename T>            inline void   set_ ## name_ (T&& buf, type_ value)                   { buffer_helpers::assert_bufsize_compile_time<size_>(buf); buffer_helpers::pack_ ## name_ (get_raw(buf), value); } \
        template <typename T>            inline type_  get_ ## name_ (const T& buf, std::size_t offs)         { buffer_helpers::assert_bufsize_runtime(buf, offs + size_); return buffer_helpers::unpack_ ## name_(get_raw(buf) + offs); } \
        template <typename T>            inline void   set_ ## name_ (T&& buf, std::size_t offs, type_ value) { buffer_helpers::assert_bufsize_runtime(buf, offs + size_);  buffer_helpers::pack_ ## name_ (get_raw(buf) + offs, value); } \
        template <std::size_t I, typename T>  inline type_  get_ ## name_ (const T& buf)                      { buffer_helpers::assert_bufsize_compile_time<I + size_>(buf); return buffer_helpers::unpack_ ## name_ (get_raw(buf) + I); } \
        template <std::size_t I, typename T>  inline void   set_ ## name_ (T&& buf, type_ value)              { buffer_helpers::assert_bufsize_compile_time<I + size_>(buf); buffer_helpers::pack_ ## name_ (get_raw(buf) + I, value); }


    DECLARE_BUFFER_GETTERS_SETTERS(  u8,  uint8_t,  1 )
    DECLARE_BUFFER_GETTERS_SETTERS( u16,  uint16_t, 2 )
    DECLARE_BUFFER_GETTERS_SETTERS( u32,  uint32_t, 4 )
    DECLARE_BUFFER_GETTERS_SETTERS( u48,  uint48_t, 6 )
    DECLARE_BUFFER_GETTERS_SETTERS( u64,  uint64_t, 8 )


    template <typename T>                      inline auto take(T&& buf, std::size_t N)                       { return buffer_helpers::methods_t<T>::       take_non_fixed(buf, N); }
    template <std::size_t N, typename T>       inline auto take(T&& buf)                                      { return buffer_helpers::methods_t<T>::       template take_fixed<0, N>(buf); }
    template <typename T>                      inline auto take(T&& buf, std::size_t I, std::size_t N)        { return buffer_helpers::methods_t<T>::       take_non_fixed(buf, I, N); }
    template <std::size_t I, std::size_t N, typename T>  inline auto take(T&& buf)                            { return buffer_helpers::methods_t<T>::       template take_fixed<I, N>(buf); }

    template <typename T>                      inline auto take(const T& buf, std::size_t N)                  { return buffer_helpers::const_methods_t<T>:: take_non_fixed(buf, N); }
    template <std::size_t N, typename T>       inline auto take(const T& buf)                                 { return buffer_helpers::const_methods_t<T>:: template take_fixed<0, N>(buf); }
    template <typename T>                      inline auto take(const T& buf, std::size_t I, std::size_t N)   { return buffer_helpers::const_methods_t<T>:: take_non_fixed(buf, I, N); }
    template <std::size_t I, std::size_t N, typename T>  inline auto take(const T& buf)                       { return buffer_helpers::const_methods_t<T>:: template take_fixed<I, N>(buf); }

    template <typename T>                      inline auto take_const(const T& buf, std::size_t N)            { return buffer_helpers::const_methods_t<T>:: take_non_fixed(buf, N); }
    template <std::size_t N, typename T>       inline auto take_const(const T& buf)                           { return buffer_helpers::const_methods_t<T>:: template take_fixed<0, N>(buf); }
    template <typename T>                      inline auto take_const(const T& buf, std::size_t I, std::size_t N)  { return buffer_helpers::const_methods_t<T>:: take_non_fixed(buf, I, N); }
    template <std::size_t I, std::size_t N, typename T>  inline auto take_const(const T& buf)                 { return buffer_helpers::const_methods_t<T>:: template take_fixed<I, N>(buf); }

    template <typename T>                      inline auto skip(T&& buf, std::size_t I)                       { return buffer_helpers::methods_t<T>::       skip(buf, I); }
    template <std::size_t I, typename T>       inline auto skip(T&& buf)                                      { return buffer_helpers::methods_t<T>::       template skip<I>(buf); }
    template <typename T>                      inline auto skip(const T& buf, std::size_t I)                  { return buffer_helpers::const_methods_t<T>:: skip(buf, I); }
    template <std::size_t I, typename T>       inline auto skip(const T& buf)                                 { return buffer_helpers::const_methods_t<T>:: template skip<I>(buf); }

    template <typename T>                      inline auto as_const(const T& buf)                             { return buffer_helpers::const_methods_t<T>:: as_const(buf); }


    template <typename T>
    static inline void fill_with(T&& buf, uint8_t value)
    {
        memset(get_raw(buf), value, buf.size());
    }


    template <typename T>
    static inline void set_zero(T&& buf)
    {
        memset(get_raw(buf), 0, buf.size());
    }


    template <std::size_t N, typename T1, typename T2 = ConstBuffer>
    static void copy(T1&& dst, const T2& src)
    {
        using U1 = typename std::remove_reference<T1>::type;
        using U2 = T2;

        constexpr bool both_fixed = U1::is_fixed && U2::is_fixed;
        static_assert(!both_fixed || (U1::fixed_size >= N && U2::fixed_size >= N));

        if (!both_fixed) {
            if (dst.size() < N) {
                throw BufferTooSmallException("Destination buffer too small");
            }

            if (src.size() < N) {
                throw BufferTooSmallException("Source buffer too small");
            }
        }
        memcpy(get_raw(dst), get_raw(src), N);
    }


    template <typename T1, typename T2 = ConstBuffer>
    static void copy(T1&& dst, const T2& src, std::size_t N)
    {
        if (dst.size() < N) {
            throw BufferTooSmallException("Destination buffer too small");
        }

        if (src.size() < N) {
            throw BufferTooSmallException("Source buffer too small");
        }

        memcpy(get_raw(dst), get_raw(src), N);
    }


    template <typename T1, typename T2 = ConstBuffer>
    static void copy(T1&& dst, const T2& src)
    {
        using U1 = typename std::remove_reference<T1>::type;
        using U2 = T2;

        constexpr bool both_fixed = U1::is_fixed && U2::is_fixed;
        static_assert(!both_fixed || U1::fixed_size == U2::fixed_size);
        if (!both_fixed) {
            if (dst.size() != src.size()) {
                throw IndexOutOfBoundsException();
            }
        }
        memcpy(get_raw(dst), get_raw(src), dst.size());
    }


    template <typename T1, typename T2 = ConstBuffer>
    static bool equals(T1&& a, T2 b)
    {
        using U1 = typename std::remove_reference<T1>::type;
        using U2 = typename std::remove_reference<T2>::type;

        constexpr bool both_fixed = U1::is_fixed && U2::is_fixed;
        static_assert(!both_fixed || (U1::fixed_size == U2::fixed_size));

        if (a.size() != b.size()) {
            return false;
        }

        return memcmp(get_raw(a), get_raw(b), a.size()) == 0;
    }


    void write_buf_to(ConstBuffer buf, std::ostream& out, std::size_t max_width = 50);
    std::ostream& operator<<(std::ostream& out, ConstBuffer buf);


    template <std::size_t N>
    inline std::ostream& operator<<(std::ostream& out, FixedConstBuffer<N> buf)
    {
        return out << skip(buf, 0);
    }

    template <std::size_t N>
    inline std::ostream& operator<<(std::ostream& out, FixedBuffer<N> buf)
    {
        return out << skip(buf, 0);
    }


} // namespace datakod_se::buffer

