#pragma once

#include <iosfwd>
#include <string>
#include <cstring>
#include <memory>
#include <exception>
#include "types.h"


namespace datakod_se::buffer {

    class BoundsException : public std::runtime_error {
        using std::runtime_error ::runtime_error;
    };


    class IndexOutOfBoundsException : public BoundsException {
        public:
            IndexOutOfBoundsException()
                : BoundsException { "Index out of bounds" }
            {
            }
    };

    class BufferTooSmallException : public BoundsException {
        public:
            BufferTooSmallException(const std::string& msg = "Buffer too small")
                : BoundsException { msg }
            {
            }
    };


    template <typename T>
    auto get_raw(T&& buf) { return buf.raw(); }


    template <std::size_t N>
    class FixedConstBuffer;       // Const buffer, where size is known at compile time

    template <std::size_t N>
    class FixedBuffer;            // Non-const buffer, where size is known at compile time

    class ConstBuffer;            // Const buffer, where size is not known at runtime 

    class Buffer;                 // Non-const buffer, where size is not known at runtime 

    template <std::size_t N>
    class StaticBuffer;

    namespace detail {
        template <typename Exception>
        static inline void runtime_assert(bool expr) {
            if (!expr) {
                throw Exception();
            }
        }

        template <typename Derived>
        class ConstBufferBase {
            public:
                inline uint8_t operator[](std::size_t i) const
                {
                    auto self = static_cast<const Derived *>(this);
                    detail::runtime_assert<IndexOutOfBoundsException>(i < self->size());
                    return get_raw(*self)[i];
                }
        };

        template <typename Derived>
        class BufferBase : public ConstBufferBase<Derived> {
            public:
                inline uint8_t& operator[](std::size_t i) {
                    auto self = static_cast<Derived *>(this);
                    detail::runtime_assert<IndexOutOfBoundsException>(i < self->size());
                    return get_raw(*self)[i];
                }
        };
    }



    template <std::size_t N>
    class FixedConstBuffer : public detail::ConstBufferBase<FixedConstBuffer<N>>
    {
        public:
            FixedConstBuffer(const void * raw)
                : raw_  { reinterpret_cast<const uint8_t *>(raw) }
            {
            }

            template <std::size_t M>
            FixedConstBuffer(const FixedConstBuffer<M>& buf)
                : raw_  { get_raw(buf) }
            {
                static_assert(M >= N, "...");
            }

            template <std::size_t M>
            FixedConstBuffer(const FixedBuffer<M>& buf)
                : raw_  { get_raw(buf) }
            {
                static_assert(M >= N, "...");
            }

            FixedConstBuffer(ConstBuffer buffer);

            static constexpr bool       is_fixed = true;
            static constexpr bool       is_const = true;
            static constexpr std::size_t     fixed_size = N;
            inline std::size_t               size() const                     { return N; }

        private:
            template <typename T> friend auto get_raw(T&& buf);
            inline const uint8_t *      raw() const                      { return raw_; }
            const uint8_t *             raw_;
    };



    template <std::size_t N>
    class FixedBuffer : public detail::BufferBase<FixedBuffer<N>>
    {
        public:
            FixedBuffer(void * raw)
                : raw_  { reinterpret_cast<uint8_t *>(raw) }
            {
            }

            static constexpr bool       is_fixed = true;
            static constexpr bool       is_const = false;
            static constexpr std::size_t     fixed_size = N;
            inline std::size_t               size() const                     { return N; }


        private:
            template <typename T> friend auto get_raw(T&& buf);
            inline uint8_t *            raw()                            { return raw_; }
            inline const uint8_t *      raw() const                      { return raw_; }
            uint8_t *                   raw_;
    };


    class ConstBuffer : public detail::ConstBufferBase<ConstBuffer>
    {
        public:
            template <std::size_t N>
            inline ConstBuffer(FixedConstBuffer<N> buf)
                : raw_  { get_raw(buf) },
                  size_ { N }
            {
            }

            template <std::size_t N>
            inline ConstBuffer(FixedBuffer<N> buf)
                : raw_  { get_raw(buf) },
                  size_ { N }
            {
            }

            inline ConstBuffer(const char * str)
                : raw_  { reinterpret_cast<const uint8_t *>(str) },
                  size_ { str != nullptr ? ::strlen(str) : 0 }
            {
            }

            inline ConstBuffer(const void * raw, std::size_t size)
                : raw_  { reinterpret_cast<const uint8_t *>(raw) },
                  size_ { size }
            {
            }

            static constexpr bool       is_fixed = false;
            static constexpr bool       is_const = true;
            static constexpr std::size_t     fixed_size = 0;
            inline std::size_t               size() const                     { return size_; }


        protected:
            template <typename T> friend auto get_raw(T&& buf);
            inline const uint8_t *      raw() const                      { return raw_; }

        protected:
            const uint8_t *             raw_;
            std::size_t                      size_;
    };




    class Buffer : public ConstBuffer
    {
        public:
            template <std::size_t N>
            inline Buffer(FixedBuffer<N> buf)
                : ConstBuffer { get_raw(buf), N }
            {
            }

            inline Buffer(void * raw, std::size_t size)
                : ConstBuffer { raw, size }
            {
            }

            static constexpr bool       is_fixed = false;
            static constexpr bool       is_const = false;
            static constexpr std::size_t     fixed_size = 0;

            inline uint8_t& operator[](std::size_t i) {
                detail::runtime_assert<IndexOutOfBoundsException>(i < size());
                return raw()[i];
            }

        private:
            inline uint8_t *            raw()                            { return const_cast<uint8_t *>(raw_); }
            inline const uint8_t *      raw() const                      { return const_cast<const uint8_t *>(raw_); }
            template <typename T> friend auto get_raw(T&& buf);
    };


} // namespace datakod_se::buffer



#include "buffer_methods.h"


namespace datakod_se::buffer {
    template <std::size_t N>
    class StaticBuffer final : public FixedBuffer<N>
    {
        public:
            StaticBuffer()
                : FixedBuffer<N> { buf_ }
            {
                set_zero(*this);
            }

        private:
            uint8_t buf_[N];
    };


    class DynamicBufferBase : public Buffer
    {
        protected:
            DynamicBufferBase(std::unique_ptr<uint8_t[]>&& x, std::size_t size)
                : Buffer { x.get(), size },
                  mem_   { std::move(x) }
            {
            }

        private:
            std::unique_ptr<uint8_t[]>  mem_;
    };


    class DynamicBuffer final : public DynamicBufferBase
    {
        public:
            DynamicBuffer(DynamicBuffer&&)      = default;
            DynamicBuffer(const DynamicBuffer&) = delete;


            DynamicBuffer(std::size_t size)
                : DynamicBufferBase { std::move(std::make_unique<uint8_t[]>(size)), size }
            {
                set_zero(*this);
            }

            DynamicBuffer(ConstBuffer buf)
                : DynamicBufferBase { std::move(std::make_unique<uint8_t[]>(buf.size())), buf.size() }
            {
                copy(*this, buf);
            }
    };

} // namespace datakod_se::buffer



namespace datakod_se::buffer {

    template <std::size_t N>
    FixedConstBuffer<N>::FixedConstBuffer(ConstBuffer buffer)
        : raw_  { get_raw(buffer) }
    {
        detail::runtime_assert<BufferTooSmallException>(buffer.size() >= N);
    }

} // namespace datakod_se::buffer




