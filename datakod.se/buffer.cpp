#include <iostream>
#include <iomanip>
#include "buffer.h"
#include "stream_state.h"

namespace datakod_se::buffer {


void write_buf_to(ConstBuffer buf, std::ostream& out, size_t max_width)
{
    size_t max_size = max_width/2;
    auto size = std::min(buf.size(), max_size);
    if (size < buf.size() && size > 0) {
        size--; // Make room for ellipsis
    }

    StreamStateGuard guard { out };
    for (size_t i = 0; i < size; i++) {
        out << std::setfill('0') << std::hex << std::setw(2) << (int) get_u8(buf,i);
    }

    if (size < buf.size() && max_width >= 2) {
        out << "..";
    }
}


std::ostream& operator<<(std::ostream& out, ConstBuffer buf)
{
    write_buf_to(buf, out);
    return out;
}


} // namespace datakod_se::buffer

