#pragma once
#include <ostream>

namespace datakod_se {


class StreamState final
{
    public:
        StreamState(const std::ostream& out)
            : flags_     { out.flags() },
              precision_ { out.precision() },
              fill_      { out.fill() }
        {
        }

        void push_to(std::ostream& out) const
        {
            out.flags(flags_);
            out.precision(precision_);
            out.fill(fill_);
        }

    private:
        const std::ios_base::fmtflags flags_;
        const std::streamsize         precision_;
        const char                    fill_;
};


class StreamStateGuard final
{
    public:
        inline StreamStateGuard(std::ostream& out)
            : out_   { out },
              state_ { out }
        {
        }

        inline ~StreamStateGuard()
        {
            state_.push_to(out_);
        }

    private:
        std::ostream&  out_;
        StreamState    state_;
};


} // namespace datakod_se

