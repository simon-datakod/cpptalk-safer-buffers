#include <iostream>
#include "datakod.se/buffer.h"
using namespace datakod_se::buffer;

void do_stuff(FixedConstBuffer<10> buf) {
    std::cout << "Buffer: (" << buf.size() << ") " << buf << "\n";
}

int main() {
    StaticBuffer<30> buf;

    fill_with(buf, 0xcc);
    do_stuff(buf);

    return 0;
}

