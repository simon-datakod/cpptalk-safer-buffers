# Small buffer library

The purpose of this library is to act as safe and essentially zero-overhead wrappers for raw C-style data buffers and pointer arithmetic. Bounds checking is done in compile time where possible, and in otherwise in runtime.

